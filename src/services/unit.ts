import type { Unit } from '@/types/Unit'
import http from './http'

function addUnit(unit: Unit) {
  return http.post('/units', unit)
}

function updateUnit(unit: Unit) {
  return http.patch(`/units/${unit.id}`, unit)
}

function delUnit(unit: Unit) {
  return http.delete(`/units/${unit.id}`)
}

function getUnit(id: number) {
  return http.get(`/units/${id}`)
}

function getUnits() {
  return http.get('/units')
}

export default { addUnit, updateUnit, delUnit, getUnit, getUnits }
